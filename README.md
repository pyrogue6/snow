# snow

For lack of snow 

## requires

- python3
- pip3
- git

## get

install this package:
```bash
$ pip3 install git+https://gitlab.com/pyrogue6/snow.git
```
update your PATH variable if needed:
```bash
$ source ~/.profile
```

## run

```bash
$ snow
```

## tested on Ubuntu 18 (4-22-2020)

## future
- move snow with mouse
- change max pile height smoothly between a range
- 3d perlin/simplex noise to give flakes an instantaneous heading