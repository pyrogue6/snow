from setuptools import setup, find_packages

tag="0.0.1"

setup(
	name='snow',
	version=tag,
	url="https://gitlab.com/pyrogue6/snow",
	download_url=None,	#f"https://gitlab.com/pyrogue6/snow/-/archive/{tag}/snow-{tag}.tar.gz",
	author="Sabrina Held",
	author_email="pyrogue6@gmail.com",
	classifiers=[
	"Environment :: Console :: Curses",
	"Programming Language :: Python :: 3.7",
	"Programming Language :: Python :: 3.8"
	],
	license="UNLICENSE",
	license_file="UNLICENSE",
	description='For lack of snow',
	long_description=open("README.md","r").read(),
	long_description_content_type="text/markdown",
	keywords="snow terminal curses",
	python_requires='>=2.7',
	packages=find_packages(),
	package_data = {},
	entry_points={
		"console_scripts": ["snow = snow.snow:main"]
	},
	install_requires=[]
)